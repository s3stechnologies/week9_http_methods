package com.s3s.training.springboot.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;
//import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.s3s.training.springboot.model.Student;

@Service
//@Component
public class StudentService {
	
	private List<Student> lisOfStudent = new ArrayList<>(Arrays.asList(new Student("Bhupesh", 20, 123),
															   new Student("Aradhana", 18, 1234),
												               new Student("Kritee", 15, 1345)));
	
	
	
	public List<Student> getListOfStudents() {
		return lisOfStudent;
	}

	public Student getStudentByid(int id) {

		for (Student student : lisOfStudent) {

			if (student.getId() == id) {
				return student;
			}

		}
		return null;

	}

	public void postStudent(Student student) {

		lisOfStudent.add(student);

	}

	public void updateStudent(int id, Student student) {
		
		
		for(Student stu: lisOfStudent) {
			
			if(stu.getId() == id) {
				
				int indexNumber = lisOfStudent.indexOf(stu);
				
				lisOfStudent.set(indexNumber, student);
				return;
			}
			
			
		}
		
		
		
	}

	public void deleteStudentByid(int id) {
		
		for(Student stu: lisOfStudent) {
			
			if(stu.getId() == id) {

				lisOfStudent.remove(stu);
				
				return;
			}
			
			
		}
		
		
	}



}
