package com.s3s.training.springboot.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {
	
	@RequestMapping("/hello")
	public String sayHello() {
		return "Hello Everyone!";
	}
	
	@RequestMapping("/hello/hi")
	public String sayHi() {
		return "Hi Everyone!";
	}

}
