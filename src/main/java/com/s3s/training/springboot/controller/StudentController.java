package com.s3s.training.springboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.s3s.training.springboot.model.Student;
import com.s3s.training.springboot.service.StudentService;

@RestController
public class StudentController {
	
	@Autowired
	private StudentService service;
	
	@RequestMapping("/allstudents")
	public List<Student> getAllStudents(){
		return service.getListOfStudents();
	}
	
	// get info or get data or dispaly data
	@GetMapping("/student/{id}")
	public Student getStudentById(@PathVariable int id)  {
		return service.getStudentByid(id);
		
	}
	
	
	// create new entry, or data or rows
	@PostMapping(value = "/student/poststudent")
	public String postStudent(@RequestBody Student student) {
		
		service.postStudent(student);
	
		return "Student got added!";
		
	}
	
	//update garnu lai put mapping garnu parcha or put http method use garnu parcha
	
	
	@PutMapping("/student/{id}")
	public String updateStudent(@PathVariable int id, @RequestBody Student student ) {
		
		service.updateStudent(id, student);
		
		return "Student Updated";
		
	}
	
	@DeleteMapping("/student/{id}")
	public String deleteStudentById(@PathVariable int id)  {
		
		service.deleteStudentByid(id);
		
		return "Student with "+id+" deleted!";
		
	}
	
	
	
}
